import 'package:flutter/material.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:bogadi_legae/themes.dart';

import 'pages/welcome_screen.dart';

Future main() async {
  /* WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      apiKey: "AIzaSyAoH1yr9x6NC6teQJSvMqSDk4lv6QigPVk",
      authDomain: "bogadilegae-5252b.firebaseapp.com",
      projectId: "bogadilegae-5252b",
      storageBucket: "bogadilegae-5252b.appspot.com",
      messagingSenderId: "637900413965",
      appId: "1:637900413965:web:2b141e88443487d3b40944");
      */

  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    // ignore: todo
    super.initState();
    currentTheme.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Bogadi Legae',
        theme: CustomTheme.lightTheme,
        darkTheme: CustomTheme.darkTheme,
        themeMode: currentTheme.currentTheme,
        home: AnimatedSplashScreen(
            duration: 3000,
            splash: Icons.bed_outlined,
            nextScreen: const WelcomeScreen(),
            splashTransition: SplashTransition.slideTransition,
            pageTransitionType: PageTransitionType.leftToRight,
            backgroundColor: Colors.orange));
  }
}
