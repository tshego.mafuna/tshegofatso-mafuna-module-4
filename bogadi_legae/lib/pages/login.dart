import 'package:flutter/material.dart';

import 'dashboard.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // backgroundColor: Colors.white30,
        appBar: AppBar(
          title: const Text("Login"),
        ),
        body: Padding(
          padding: const EdgeInsets.all(28.0),
          child: Column(
            children: [
              Container(
                alignment: Alignment.center,
                child: Image.asset('images/userProfile.png',
                    height: 161, width: 161),
              ),
              const Text(
                'Login In here!',
                style: TextStyle(fontSize: 28, height: 2),
              ),
              TextFormField(
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'Enter your username',
                ),
              ),
              TextFormField(
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'Enter your password',
                ),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(5, 25, 50, 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    TextButton(
                      style: ElevatedButton.styleFrom(
                        // Foreground color
                        onPrimary:
                            Theme.of(context).colorScheme.onSecondaryContainer,
                        // Background color
                        primary:
                            Theme.of(context).colorScheme.secondaryContainer,
                      ).copyWith(elevation: ButtonStyleButton.allOrNull(0.0)),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: const Text(
                        'Cancel',
                      ),
                    ),
                    TextButton(
                      style: ElevatedButton.styleFrom(
                        // Foreground color
                        onPrimary:
                            Theme.of(context).colorScheme.onSecondaryContainer,
                        // Background color
                        primary:
                            Theme.of(context).colorScheme.secondaryContainer,
                      ).copyWith(elevation: ButtonStyleButton.allOrNull(0.0)),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const Dashboard()),
                        );
                      },
                      child: const Text(
                        'Login',
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ));
  }
}
