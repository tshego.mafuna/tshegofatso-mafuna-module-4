import 'package:flutter/material.dart';

class About_page extends StatefulWidget {
  const About_page({
    Key? key,
  }) : super(key: key);

  @override
  State<About_page> createState() => _About_pageState();
}

class _About_pageState extends State<About_page> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          width: 380,
          margin: const EdgeInsets.all(10.0),
          child: const Text(
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec arcu magna, varius vitae hendrerit ac, pretium sed nulla. Nullam eleifend mauris a risus porta sollicitudin non ut felis. Mauris sed quam ut sapien posuere vulputate. Aliquam condimentum, mauris in eleifend interdum, nisl magna tristique urna, in hendrerit nisi ipsum in ligula. Mauris commodo at leo non porta. Nullam quis ipsum ante. Sed lobortis, dui suscipit volutpat volutpat, lorem enim facilisis libero, at ultrices arcu mi id ligula. Morbi et diam finibus, vulputate turpis sit amet, pellentesque metus.  us"),
        ),
      ),
    );
  }
}
